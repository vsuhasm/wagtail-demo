from __future__ import absolute_import, unicode_literals

from django.db import models

from wagtail.wagtailcore.models import Page
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailadmin.edit_handlers import FieldPanel

from blog.models import BlogPage


class HomePage(Page):
    # Db models
    body = RichTextField(blank=True)

    # Editer panels
    content_panels = Page.content_panels + [
        FieldPanel('body', classname="full")
    ]

    def get_context(self, request):
        blogs = BlogPage.objects.live().order_by('-date')[:3]
        context = super(HomePage, self).get_context(request)
        context['blogs'] = blogs

        return context
